﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseItemObject : ScriptableObject, IHaveId
{
    [SerializeField] private int id;

	int IHaveId.id => id;
}

public interface IHaveDescription
{
    string GetDescription();
}

public interface IHaveImageAndNum
{
	Sprite GetImage();
	int GetNum();
}

public interface IHaveId
{
	int id { get; }
}