﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName =  "New sro", menuName = "Same menu/obj")]
public class TestObj : BaseObject
{
    [SerializeField] public string desc;
    [SerializeField] public int cost;
    [SerializeField] public Sprite img;
}
public class BaseObject : ScriptableObject
{
    
}
