﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public abstract class SharedWindowContent : MonoBehaviour
{
    private const string LINE_DECOR_ANIMATION_NAME = "LineDecor";
    private const string BUTTON_CANVAS_NAME_OBJECT = "ButtonCanvas";
    private const string CLOSE_BUTTON_NAME_OBJECT = "CloseButton";

    [SerializeField] private TextMeshProUGUI _headerText;
    [SerializeField] private Animation _animation;
    [SerializeField] private Button _closeButton;

    [SerializeField] protected string _contentHeaderText;

    [SerializeField]private OpenCanvasOnButton _openCanvasOnButton;

	public virtual void Initialization()
    {
        _openCanvasOnButton = GameObject.Find(BUTTON_CANVAS_NAME_OBJECT).GetComponent<OpenCanvasOnButton>();
        var obj = FindObject(this.gameObject.transform);
        obj.GetComponent<Button>().onClick.AddListener(() => CloseCanvas());
    }

    private void CloseCanvas()
	{
        _openCanvasOnButton.openingCanvas = !_openCanvasOnButton.openingCanvas;
        Destroy(this.gameObject);
    }

    private Transform FindObject(Transform parent)
    {
        Transform obj = parent.Find(CLOSE_BUTTON_NAME_OBJECT);
        if( obj != null)
            return obj;

        for (int i = 0; i < parent.childCount; i++)
        {
            obj = parent.GetChild(i).Find(CLOSE_BUTTON_NAME_OBJECT);
            if (obj != null)
                return obj;
        }
        return null;
    }
    
    protected void SetHeaderText() => _headerText.text = _contentHeaderText;
    protected void StartAnimation() => _animation.IsPlaying(LINE_DECOR_ANIMATION_NAME);

}