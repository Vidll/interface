﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WinItem", menuName = "Item/win")]
public class WinItemObj : BaseItemObject, IHaveImageAndNum
{
    [SerializeField] public Sprite img;
    [SerializeField] public int num;

    public Sprite GetImage() => img;
    public int GetNum() => num;
}
