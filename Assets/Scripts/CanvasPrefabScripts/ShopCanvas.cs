﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopCanvas : SharedWindowContent
{
	[SerializeField] private GameObject _scrollPrefab;
    [SerializeField] private GameObject _parent;

    private ShopItemObj[] _array;

	public override void Initialization()
	{
		base.Initialization();

        _contentHeaderText = "Shop";
        SetHeaderText();
        StartAnimation();

        _array = Resources.LoadAll<ShopItemObj>("Items/");

        CreateContentOnScrollPanel();
    }

    protected void CreateContentOnScrollPanel()
    {
        foreach (var item in _array)
        {
            var obj = Instantiate(_scrollPrefab, _parent.transform);
            var cmp = obj.GetComponent<SharedScrollinPrefab>();
            cmp.SetData(item);
        }
    }
}
