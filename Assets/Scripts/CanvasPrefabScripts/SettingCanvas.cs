﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingCanvas : SharedWindowContent
{
	public override void Initialization()
	{
		base.Initialization();
		_contentHeaderText = "Setting";
		SetHeaderText();
		StartAnimation();
	}
}
