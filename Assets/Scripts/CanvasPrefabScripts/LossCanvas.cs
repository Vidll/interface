﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LossCanvas : SharedWindowContent
{
	public override void Initialization()
	{
		base.Initialization();
		_contentHeaderText = "You Loss";
		SetHeaderText();
		StartAnimation();
	}
}