﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InfoCanvas : SharedWindowContent
{
	[SerializeField] private TextMeshProUGUI _infoTextContentObject1;
	[SerializeField] private TextMeshProUGUI _infoTextContentObject2;

	[SerializeField] private string _infoTextContent1;
	[SerializeField] private string _infoTextContent2;

	public override void Initialization()
	{
		base.Initialization();

		_contentHeaderText = "Info panel";
		SetHeaderText();
		StartAnimation();
		SetTextContent();
	}

	private void SetTextContent()
	{
		_infoTextContentObject1.text = _infoTextContent1;
		_infoTextContentObject2.text = _infoTextContent2;
	}
}
