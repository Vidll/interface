﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCanvas : SharedWindowContent
{
    [SerializeField] private GameObject _scrollPrefab;
    [SerializeField] private GameObject _parent;

    private WinItemObj[] _array;

	public override void Initialization()
	{
		base.Initialization();

        _contentHeaderText = "You Win";
        SetHeaderText();
        StartAnimation();

        _array = Resources.LoadAll<WinItemObj>("Items/");

        CreateContentOnScrollPanel();
    }

    protected void CreateContentOnScrollPanel()
    {
		foreach (var item in _array)
		{
            var obj = Instantiate(_scrollPrefab, _parent.transform);
            var cmp = obj.GetComponent<SharedScrollinPrefab>();
            cmp.SetData(item);
		}
    }
}
