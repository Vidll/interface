﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShopItem", menuName ="Item/shop")]
public class ShopItemObj : BaseItemObject, IHaveDescription , IHaveImageAndNum
{
    [SerializeField] public Sprite img;
    [SerializeField] public int cost;
    [SerializeField] public string desc;

	public string GetDescription() => desc;
    public Sprite GetImage() => img;
    public int GetNum() => cost;
}
