﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OpenCanvasOnButton : MonoBehaviour
{
    [SerializeField] private Button _buttonPrefab;
    [SerializeField] private SharedWindowContent[] _canvasObjects;

    public bool openingCanvas = false;

	private void Start()
	{
        _canvasObjects = Resources.LoadAll<SharedWindowContent>("Canvas/");
        CreateButtons();
	}

    private void CreateButtons()
	{
        foreach (var canvas in _canvasObjects) 
        {
            var button = Instantiate(_buttonPrefab);
            button.gameObject.SetActive(true);
            button.transform.parent = _buttonPrefab.transform.parent;
            button.transform.localScale = Vector3.one;
            AddListener(button, canvas);
        }
	} 
    private void AddListener(Button button, SharedWindowContent canvas)
	{
        button.onClick.AddListener(() => CreateCanvas(canvas));
        button.GetComponentInChildren<TextMeshProUGUI>().text = canvas.name;
	}

	public void CreateCanvas(SharedWindowContent canvasPref)
    {
        if (!openingCanvas)
        {
            Create(canvasPref);
        }
    }

    private SharedWindowContent Create(SharedWindowContent canvasPref)
    {
        var obj = Instantiate(canvasPref.gameObject, canvasPref.transform.position, canvasPref.transform.rotation);
        openingCanvas = !openingCanvas;
        var x = obj.GetComponent<SharedWindowContent>();
        x.Initialization();
        return x;
    }
}
