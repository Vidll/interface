﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class SharedScrollinPrefab : MonoBehaviour
{ 
    [SerializeField] protected Image _imageObject;
    [SerializeField] protected TextMeshProUGUI _costObject;
    [SerializeField] protected TextMeshProUGUI _textDescription;
    [SerializeField] protected Button _button;

    public virtual void SetData(BaseItemObject obj)
    {
        if(obj is IHaveImageAndNum objSet)
		{
            _imageObject.sprite = objSet.GetImage();
            _costObject.text = objSet.GetNum().ToString();
		}

        if (obj is IHaveDescription objDesc)
		{
            _textDescription.text = objDesc.GetDescription();
            _button.onClick.AddListener(() => Debug.Log("Click on button"));
        }  
    }

    protected void SetSize() => transform.localScale = Vector3.one;
     
}